# openssl-wrapping-key-lcl

Docker Image with OpenSSL 1.1.1d patched 
To wrap private keys to import in HSM

- Create or store your private key in local folder
- To wrap private key called `priv.pem` run docker image with the following command : `docker run -v $(pwd):/work -it registry.gitlab.com/linxo-public/openssl-wrapping-key-lcl priv.pem`
- Output will give you the key wrapped

`Key priv.pem wrapped in file priv.pem_wrapped`

https://aws.amazon.com/premiumsupport/knowledge-center/cloudhsm-import-keys-openssl/
