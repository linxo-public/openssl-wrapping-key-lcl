#!/usr/bin/env sh
source /root/.profile
PUBLICKEY=/root/public.pem
cd /work/
rm -f ephemeral_*
rm -f *_encrypt
rm -f *_wrapped
OPENSSL_V111 rand -out ephemeral_aes 32
EPHEMERAL_AES_HEX=$(hexdump -v -e '/1 "%02X"' < ephemeral_aes)
OPENSSL_V111 pkcs8 -topk8 -inform PEM -outform DER -in $1 -out $1.der -nocrypt
echo "Ephemeral AES key is :$EPHEMERAL_AES_HEX"
OPENSSL_V111 enc -id-aes256-wrap-pad -K $EPHEMERAL_AES_HEX -iv A65959A6 -in $1.der -out $1_encrypt
OPENSSL_V111 pkeyutl -encrypt -in ephemeral_aes -out ephemeral_wrapped -pubin -inkey $PUBLICKEY -pkeyopt rsa_padding_mode:oaep -pkeyopt rsa_oaep_md:sha1 -pkeyopt rsa_mgf1_md:sha1
cat ephemeral_wrapped $1_encrypt > $1_wrapped
echo -- OXLIN Public key --
cat $PUBLICKEY
echo ""
if [ $? == 0 ]; then
echo "Key $1 wrapped in file $1_wrapped"
else
echo "Key wrapping failed"
fi
